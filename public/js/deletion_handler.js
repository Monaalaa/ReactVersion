$(document).ready(function() {

  //on clicking either the "Select Individual" or "Select All" option.
  $('#remove_dropdown li').on('click', function() {
    //showing checkboxes next to the employees pics.
    $('.checkbox').css('display', 'inline');


  });


  //now, we need to specify which option was chosen. If select all was chosen, we mush check all boxes.
  $('#sel_all').on('click', function() {
    $('.checkbox').prop('checked', true);
    $('#submitButton').css('display', 'inline'); //displaying delete button.
  });

  $('#sel_i').on('click', function() {
    $('.checkbox').prop('checked', false);
    $('#submitButton').css('display', 'none');
  });


  //we need to detect the change of the checkbox.
  $("#example tbody").on("click", "input:checkbox", function(event) {
    if ($('input:checked').length > 0) {
      $('#submitButton').css('display', 'inline');
    } else {
      $('#submitButton').css('display', 'none');
    }
  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27) { // escape key maps to keycode `27`
      //undo the whole process.
      $('#submitButton').css('display', 'none');
      $('.checkbox').css('display', 'none');
    }
  });



  $('#deletion_permission').on('click', function() {
    var datatable = $('#example').DataTable();
    var deleted_emp = [];
    $('tr').each(function() {
      if ($(this).find('.checkbox').prop('checked')) {
        //first delete the checked row on the view.
        datatable.row($(this)).remove();

        //second keep it in an array to send to the controller.
        //console.log($(this).index() + "val" + $(this).find("td:first").text() + "ok");
        deleted_emp.push($(this).find("td:first").text());




      }
    });

    //check if something is actually selected.

    if (Array.isArray(deleted_emp) && deleted_emp.length > 0) {
      send_delete_emp_req(deleted_emp);
      //after iterating the table. show the effect.
      datatable.draw();
    }



    // Get the snackbar DIV
    var notification = document.getElementById("snackbar");

    // Add the "show" class to DIV
    notification.className = "show";

    // After 3 seconds, remove the show class from DIV
    setTimeout(function() {
      notification.className = notification.className.replace("show", "");
    }, 3000);


  });









});



function send_delete_emp_req(deleted_emp) {
  var request = new XMLHttpRequest();
  var url = 'http://localhost:8081/catalog/empdr/deletion';


  request.open('POST', url, true);
  //request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
  /*request.onload = function () {

 	  	if (request.status >= 200 && request.status < 400) {
			var json = JSON.parse(this.response);


			populate(json.data);
	  	} else {
	    		Alert("ERROR! Cannot connect to server.");
	  	}
	}*/

  //converting array to json object.
  var json_obj = JSON.stringify(deleted_emp);
  request.send(json_obj);







};
