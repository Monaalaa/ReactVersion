$(document).ready(function() {
  var segments = window.location.href.split('/');
  var id = segments.pop() || segments.pop(); // handle potential trailing slash

/*
  request_employee_data(id);
*/

  $('#deletion_permission').on('click', function(){
    //call employees controller to delete employee of given id.

    window.location.href = 'http://localhost:8081/catalog/empdr/del/' + id;
  });
  



});

(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#sideNav'
  });

})(jQuery); // End of use strict


function request_employee_data(id) {
  //make an XMLHttpRequest to get employees data of id 'id' from database.
 // get(/employee/:id)

  var request = new XMLHttpRequest();
  var url = 'http://localhost:8000/employee/' + id;


  request.open('GET', url, true);
  request.onload = function() {

    if (request.status >= 200 && request.status < 400) {
      var json = JSON.parse(this.response);



    } else {
      alert("ERROR! Cannot connect to server.");
    }
  }

  request.send();
};
