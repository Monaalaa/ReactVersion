import React, { Component } from "react";
import HeaderPages from "./Header";
import Input from "./InputComponent";
import PagingElement from "./Paging";
import ButtonModel from "./ModelButtons";
import Searchbar from "./SearchPart";
import Droplist from "./DropListElement";
import $ from "jquery";
import DataTable from "datatables.net";

import "./Employee.css";

class PageBody extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <HeaderPages pageHeader="Projects Dataset" />
        <div id="form">
          <nav id="search_form" className="navbar navbar-default col-md-8">
            <div className="container-fluid">
              <Searchbar />
              <div
                className="collapse navbar-collapse"
                id="bs-example-navbar-collapse-1"
              >
                <Droplist index="1" dropName="First_Name" />
                <Droplist index="2" dropName="Filter by" />
                <Input />
                <Droplist index="3" dropName="Remove" />
                <ButtonModel
                  ModelTitle="ALERT!"
                  ModelSubmit="YES,I'm totally sure"
                />
              </div>
            </div>
          </nav>
        </div>
        <div class="wrapper">
          <table
            id="example"
            className="table table-striped table-bordered"
            align="center"
          >
            <thead>
              <tr>
                <th className="text-center">Project</th>
                <th className="text-center">from</th>
                <th className="text-center">to</th>
                <th className="text-center">Description</th>
              </tr>
            </thead>
            <tbody />
          </table>
          <PagingElement />
        </div>
      </div>
    );
  }
}
class Project extends Component {
  render() {
    return <PageBody />;
  }
}
export default Project;
