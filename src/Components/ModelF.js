import React, { Component } from "react";

class ModelFooter extends Component {
    render() {
      return (
        <div className="modal-footer">
          <button
            id="cancel"
            type="button"
            className="btn btn-secondary"
            data-dismiss="modal"
          >
            Cancel
          </button>
          <button
            type="button"
            className="btn btn-primary"
            id="deletion_permission"
            data-dismiss="modal"
          >
            {this.props.footerButton}
          </button>
        </div>
      );
    }
  }
export default ModelFooter;
  