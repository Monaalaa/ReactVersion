import React, { Component } from "react";

class Droplist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      menu_1: ["Last_Name", "E-mail", "Contact_Number", null],
      menu_2: ["Freezing", "Part/Full time", "No longer working", "Trainee"],
      menu_3: ["Select Individuals", "Select All", null, null]
    };
  }

  render() {
    let menus = Array(4).fill(null);
    if (this.props.index && this.props.index === "1") {
      menus = this.state.menu_1;
    } else if (this.props.index && this.props.index === "2") {
      menus = this.state.menu_2;
    } else if (this.props.index && this.props.index === "3") {
      menus = this.state.menu_3;
    }
    return (
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <button
            id="delete"
            type="button"
            className="btn btn-default dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {this.props.dropName}
            <span class="caret" />
          </button>
          <ul className="dropdown-menu">
            <li>
              {" "}
              <a href="#">{menus[0]}</a>
            </li>
            <li>
              {" "}
              <a href="#">{menus[1]}</a>
            </li>
            <li>
              {" "}
              <a href="#">{menus[2]}</a>
            </li>
            <li>
              {" "}
              <a href="#">{menus[3]}</a>
            </li>
          </ul>
        </li>
      </ul>
    );
  }
}

export default Droplist;
