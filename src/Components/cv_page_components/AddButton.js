import React, {Component} from 'react';



class AddButton extends Component {
  render() {
    var buttonStyle ={
      float : "left";
    }
    return(
      <button id="add" type="button" class="btn btn-default pull-left" style={buttonStyle} data-toggle="modal" data-target="#addskills">
        <i class="fa fa-plus"></i>
      </button>
    );
  }
}


export default AddButton;
