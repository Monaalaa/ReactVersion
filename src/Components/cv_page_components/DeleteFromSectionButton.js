import React, {Component} from 'react';


class DeleteFromSectionButton extends Component {


  render() {
    return (
      <button id="trash" type="button" class="btn btn-default" style="float:right;">
        <i class="fa fa-trash"></i>
      </button>
    );
  }
}

export default DeleteFromSectionButton;
