import React, { Component } from 'react';



class EditExperienceSection extends Component {


  render() {
    var buttonStyle = {
      floatLeft : "left"
    }, spanStyle = {
      maxWidth : "100%"
    }

    return (
      <React.Fragment>
      <button id="add" type="button" class="btn btn-default pull-left" style={buttonStyle} data-toggle="modal" data-target="#addexperience">
        <i class="fa fa-plus"></i>
      </button>
      <div class="modal fade" id="addexperience" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="langTitle">
                <b>Experience</b>
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="eform" class="well form-horizontal">
                <fieldset>
                  <div class="form-group">
                    <label class="col-md-4 control-label">Place</label>
                    <div class="col-md-8 inputGroupContainer">
                      <div class="input-group">
                        <span class="input-group-addon" style={spanStyle}>
                          <i class="glyphicon glyphicon-list"></i>
                        </span>
                        <input id="fullName" name="fullName" placeholder="Company name" class="form-control" required="true" value="" type="text"/>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">to</label>
                    <div class="col-md-8 inputGroupContainer">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="glyphicon glyphicon-calendar"></i>
                        </span>
                        <input id="fullName" name="fullName" class="form-control" required="true" value="" type="date"/>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">from</label>
                    <div class="col-md-8 inputGroupContainer">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="glyphicon glyphicon-calendar"></i>
                        </span>
                        <input id="fullName" name="fullName" class="form-control" required="true" value="" type="date"/>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label">role</label>
                    <div class="col-md-8 inputGroupContainer">
                      <div class="input-group">
                        <span class="input-group-addon" style={spanStyle}>
                          <i class="glyphicon glyphicon-list"></i>
                        </span>
                        <input id="fullName" name="fullName" placeholder="job" class="form-control" required="true" value="" type="text"/>
                      </div>
                    </div>
                  </div>
                </fieldset>
              </form>
              <div class="modal-footer">
                <button id="cancel" type="button" class="btn btn-secondary" data-dismiss="modal"><b>Cancel</b></button>
                <button type="button" class="btn btn-primary" id="addSubmit" data-dismiss="modal"><b>Add</b></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      </React.Fragment>
    );
  }
}

export default EditExperienceSection;
