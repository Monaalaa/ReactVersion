import React, { Component } from 'react';



class ExperienceBody extends Component {


  render() {
    const pStyle = {
      fontSize : "20px"
    };

    function ExperienceList(props) {
      const experience = props.experience;
      const experienceList = experience.map((experience) =>

          <div class="resume-item d-flex flex-column flex-md-row mb-5">
            <div class="resume-content mr-auto">
              <h3 class="mb-0">{experience.roleDescription}</h3>
              <p style={pStyle}>{experience.place}</p>
            </div>
            <div class="resume-date text-md-right" style={pStyle}>
              <span class="text-primary">{experience.from} - {experience.to}</span>
              <br/>
            {/*  <button id="trash" type="button" class="btn btn-default pull-right">
                <i class="fa fa-trash"></i>
              </button>
              <button id="editButton" type="button" class="btn btn-default">
                <i class="fa fa-edit"></i>
              </button>*/}

            </div>
          </div>

      );

      return (<React.Fragment>{experienceList}</React.Fragment>);
    }
    const experience = [{roleDescription:'SOCIAL WORKER',place:'China', from:'2018-05-21T22:00:00.000Z', to:'2018-05-09T22:00:00.000Z'}, {roleDescription:'bye',place:'bye', from:'bye', to:'bye'}];
    return (<ExperienceList experience={experience}/>);
  }
}

export default ExperienceBody;
