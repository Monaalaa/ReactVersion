import React, { Component } from 'react';

//experience body
import ExperienceBody from './ExperienceBody';
//editing
import EditExperienceSection from './EditExperienceSection';

class ExperienceSection extends Component {

  render() {
    return (
      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="experience">
        <div class="my-auto">
          <h2 class="mb-5">Experience</h2>
          <ExperienceBody />
          <br />
      {/* <EditExperienceSection/> */}
        </div>
      </section>
    );
  }
}

export default ExperienceSection;
