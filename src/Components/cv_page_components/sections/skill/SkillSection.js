import React, {Component} from 'react';



class SkillSection extends Component {
  render() {
    var spanStyle = {
      maxWidth : "100%"
    }
    return(
      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="skills">
        <div class="my-auto">
          <h2 class="mb-5">Skills</h2>
          <List />
        </div>
      </section>
    );
  }
}


export default SkillSection;
