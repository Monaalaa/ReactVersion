import React, {Component} from 'react';


class BackButton extends Component {


  render() {
    return (
        <button id="edbutton" class="w3-button w3-round-xlarge">Back</button>
    );
  }
}

export default BackButton;
