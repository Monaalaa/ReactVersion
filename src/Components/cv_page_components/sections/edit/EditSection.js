import React, {Component} from 'react';

//back button
import BackButton from './BackButton';

//delete button
import DeleteCvButton from './DeleteCvButton';

//are you sure Modal
import AreUSure from '../../AreUSure'

class EditSection extends Component {


  render() {

    return (
      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="edit">
        <div class="my-auto">
          <h2 class="mb-5">Edit</h2>
          <BackButton />

          <DeleteCvButton />

          <AreUSure entryType="employee"/>
        </div>
      </section>
    );
  }
}

export default EditSection;
