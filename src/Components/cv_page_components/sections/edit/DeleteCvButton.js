import React, {Component} from 'react';


class DeleteCvButton extends Component {


  render() {
    var buttonStyle = {
      marginTop : "5px",
      marginLeft : "5%"
    }
  

    return (
      <button id="delbutton" type="button" class="w3-button w3-round-xlarge" data-toggle="modal" data-target="#delete_confirmation"
        style={buttonStyle}>Delete</button>
    );
  }
}

export default DeleteCvButton;
