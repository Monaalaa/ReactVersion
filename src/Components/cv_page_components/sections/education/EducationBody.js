import React, {Component} from 'react';




class EducationBody extends Component {


  render() {
    var pStyle = {
      fontSize : "20px"
    };

    function EducationList(props) {
      const education = props.education;
      const educationList = education.map((education) =>

      <div class="resume-item d-flex flex-column flex-md-row mb-5">
        <div class="resume-content mr-auto">
          <h3  class="mb-0">{education.university}</h3>
          <p style={pStyle}>GPA: {education.grade}</p>
        </div>
        <div class="resume-date text-md-right"style={pStyle}>
          <span class="text-primary">{education.from} - {education.to}</span>
          <br/>
        { /* <button id="trash" type="button" class="btn btn-default pull-right">
            <i class="fa fa-trash"></i>
          </button>
          <button id="editButton" type="button" class="btn btn-default">
            <i class="fa fa-edit"></i>
          </button> */}
        </div>
      </div>

      );

      return (<React.Fragment>{educationList}</React.Fragment>);
    }
    const education = [{university:'hi',grade:'hi', from:'hi', to:'hi'}, {university:'bye',grade:'bye', from:'bye', to:'bye'}];
    return (<EducationList education={education}/>);
  }
}

export default EducationBody;
