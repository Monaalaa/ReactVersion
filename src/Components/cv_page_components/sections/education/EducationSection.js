import React, {Component} from 'react';


//education body.
import EducationBody from './EducationBody';


class EducationSection extends Component {


  render() {
    return (
      <section class="resume-section p-3 p-lg-5 d-flex flex-column" id="education">
        <div class="my-auto">
          <h2 class="mb-5">Education</h2>
          <EducationBody />
        </div>
      </section>
    );
  }
}

export default EducationSection;
