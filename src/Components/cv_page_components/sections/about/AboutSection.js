import React, { Component } from 'react';

//header About
import HeaderAboutSection from './HeaderAboutSection';

//social media icons
import SocialContactIcons from './SocialContactIcons';

class AboutSection extends Component {

  render() {
    return (
        <section class="resume-section p-3 p-lg-5 d-flex d-column" id="about">
          <div class="my-auto">
            <HeaderAboutSection firstname='Bassent' lastname='Zaghloul' address='ok' email='bassentzaghloul@gmail.com'/>
            <SocialContactIcons fb_account='fb.com/bassentzaghloul' twitter_account='twitter.com/bassentzaghloul'/>
          </div>
        </section>
    );
  }
}

export default AboutSection;
