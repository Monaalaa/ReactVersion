import React, { Component } from 'react';

//name subsection
import NameHeaderAboutSection from './NameHeaderAboutSection';
//subheader subsection
import SubHeaderAboutSection from './SubHeaderAboutSection';

class HeaderAboutSection extends Component {

  render() {
    return (
      <div class="my-auto">
        <NameHeaderAboutSection firstname={this.props.firstname} lastname={this.props.lastname}/>
        <SubHeaderAboutSection address={this.props.address} email={this.props.email}/>
      </div>
    );
  }
}

export default HeaderAboutSection;
