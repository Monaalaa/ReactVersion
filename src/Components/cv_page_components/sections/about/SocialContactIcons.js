import React, { Component } from 'react';

//social icons
import SocialIcon from './SocialIcon';

class SocialContactIcons extends Component {

  render() {
    return (
        <ul class="list-inline list-social-icons mb-0">
          { this.props.fb_account &&
            <SocialIcon account={this.props.fb_account} classname="fa fa-facebook fa-stack-1x fa-inverse"/>
          }

          { this.props.twitter_account &&
            <SocialIcon account={this.props.twitter_account} classname="fa fa-twitter fa-stack-1x fa-inverse" />
          }

          { this.props.linkedin_account &&
            <SocialIcon accout={this.props.linkedin_account} classname="fa fa-linkedin fa-stack-1x fa-inverse" />
          }

          { this.props.github_account &&
            <SocialIcon account={this.props.github_account} classname="fa fa-github fa-stack-1x fa-inverse" />
          }
        </ul>
    );
  }
}

export default SocialContactIcons;
