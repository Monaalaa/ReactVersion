import React, { Component } from 'react';



class SocialIcon extends Component {

  render() {
    return (
      <li class="list-inline-item">
        <a title='Visit account!' href={this.props.account}>
          <span class="fa-stack fa-lg">
            <i class="fa fa-circle fa-stack-2x"></i>
            <i class={this.props.classname}></i>
          </span>
        </a>
      </li>
    );
  }
}

export default SocialIcon;
