import React, {Component} from 'react';



class InsertForm extends Component {
  render() {
    var spanStyle = {
      maxWidth : "100%"
    }
    return(
      <div class="modal fade" id={this.props.id} tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="langTitle">
                <b>{this.props.title}</b>
              </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <form id="eform" class="well form-horizontal">
                <fieldset>
                  <div class="form-group">
                    <label class="col-md-5 control-label">Add {this.prop.listof}</label>
                    <div class="col-md-8 inputGroupContainer">
                      <div class="input-group">
                        <span class="input-group-addon" style={spanStyle}>
                          <i class="glyphicon glyphicon-education"></i>
                        </span>
                        <input id="fullName" name="fullName" placeholder={this.props.listof} class="form-control" required="true" value="" type="text">
                      </div>
                    </div>
                  </div>
                </fieldset>
              </form>

              <div class="modal-footer">
                <button id="cancel" type="button" class="btn btn-secondary" data-dismiss="modal"><b>Cancel</b></button>
                <button type="button" class="btn btn-primary" id="addSubmit" data-dismiss="modal"> <b>Add</b></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default InsertForm;
