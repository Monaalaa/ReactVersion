import React, {Component} from 'react';



class AreUSure extends Component {


//expected input to be string.
  deleteEntry(entryType) {
    if(entryType === 'project') {

    } else if(entryType === 'employee'){
      //now it's a cv
      //we need to get the id and make an ajax request to the server to delete the employee
      //with the given id, then we need to redirect the user to the 404 page.

      //1- ajax request
      const axios = require('axios');

      axios.get('http://localhost:8081/catalog/empdr/del/' + this.props.id)
      .then(function (response) {
        // handle success
        console.log('success!');

      //2- redirect to 404 page.




      })
      .catch(function (error) {
        // handle error
        console.log('error!');
      })
      .then(function () {
        // always executed
      });








    } else {
      alert(entryType);
    }
  }

  render() {
    const entryType = this.props.entryType;
    return (
      <div class="modal fade" id="delete_confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">ALERT!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <h id="alert">
                ALERT! Are you sure you want to permenantly delete {this.props.firstname} {/* can also be a project's name*/}s data from the system?!</h>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary" id="deletion_permission" data-dismiss="modal" onClick={() => this.deleteEntry(entryType)}> YES, Im totally sure.</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AreUSure;
