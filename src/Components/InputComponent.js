import React, { Component } from "react";

class Input extends Component {
    render() {
      return (
        <form className="navbar-form navbar-left">
          <div className="form-group">
            <input type="text" className="form-control" placeholder="Search" />
          </div>
        </form>
      );
    }
  }
export default Input;
