import React, { Component } from 'react';
import './CVPage.css';


//cv fixed nav bar to the left of the page.
import FixedNavBar from './cv_page_components/FixedNavBar';

//About section.
import AboutSection from './cv_page_components/sections/about/AboutSection';

//Experience section.
import ExperienceSection from './cv_page_components/sections/experience/ExperienceSection';

//Education section.
import EducationSection from './cv_page_components/sections/education/EducationSection';

//Edit section.
import EditSection from './cv_page_components/sections/edit/EditSection';

class CVPage extends Component {
  render() {
    return (
      
      <React.Fragment>
      <FixedNavBar/>
      <div class="container-fluid p-0">
        <AboutSection/>
        <ExperienceSection/>
        <EducationSection />
        <EditSection />
      </div>
      </React.Fragment>
      );
  }
}
export default CVPage;
