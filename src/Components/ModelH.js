import React, { Component } from "react";

class ModelHeader extends Component {
    render() {
      return (
        <div className="modal-header">
          <h5 className="modal-title" id="alert">
            <b>{this.props.headerTitle}</b>
          </h5>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-label="Close"
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      );
    }
  }
export default ModelHeader;