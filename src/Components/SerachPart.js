import React, { Component } from "react";

class Searchbar extends Component {
  render() {
    return (
      <div className="navbar-header">
        <button
          type="button"
          className="navbar-toggle collapsed"
          data-toggle="collapse"
          data
          target="#bs-example-navbar-collapse-1"
          aria-expanded="false"
        >
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar" />
          <span className="icon-bar" />
          <span className="icon-bar" />
        </button>
        <a id="searchbar" className="navbar-brand" href="#">
          Search
        </a>
      </div>
    );
  }
}

export default Searchbar;
