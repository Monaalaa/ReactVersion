import React, { Component } from "react";
import { Link } from 'react-router-dom'
import './Employee.css'

class HeaderPages extends Component {

  render() {
    return (
      <div>
        <p id="header">
          <b>{this.props.pageHeader}</b>
        </p>
        <div id="pages" align="center">
      
        <a><Link to='/'>Home</Link></a>
        <a><Link to='/Employee'>Employee</Link></a>
        <a><Link to='/Project'>Project</Link></a>
        </div>
      </div>
    );
  }
}

export default HeaderPages;
