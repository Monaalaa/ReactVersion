import React, { Component } from "react";
import HeaderPages from "./Header";
import Input from "./InputComponent";
import ButtonModel from "./ModelButtons";
import Searchbar from "./SearchPart";
import Droplist from "./DropListElement";
import $ from "jquery";
import DataTable from "datatables.net";
import PagingElement from "./Paging";
import Link from 'react-router-dom';
import "./Employee.css";

const wrapperStyle = {
  width :'68%',
  margin : '0 0 0 15%',
}
class Table extends Component {
  render(){
    function dataList(props) {
      const Employeedata = props.data;
      const dataList = Employeedata.map((Employeedata) =>
      <tr>
        <td>
          </td>
        <td>
         <div class="person">
            <input id="select" type="checkbox" aria-label="..."/>
            <div class="img-container">
              <img src="http://via.placeholder.com/350x150" class="employee-avatar"/>
            </div>
            <p class="person-name">{Employeedata.name.firstname}{Employeedata.name.lastname}</p>
            <svg id="circle" height="50" width="50">
              <circle cx="25" cy="27" r="5" stroke="red" stroke-width="3" fill="red" />
            </svg>
            </div>
        </td>
      </tr>
      );
      return (<React.Fragment>{dataList}</React.Fragment>);
  }
  return (<dataList Employeedata={this.props.data}/>);
  }
}

class PageBody extends Component {
  constructor(props){
    super(props);
    this.populate_table = this.populate_table.bind(this);
    this.state = {
      data:{}
    };
  }
  populate_table(json_obj) {
    var request = new XMLHttpRequest();
    var url =
      "http://localhost:8000/employees/" +
      json_obj._page +
      "/" +
      json_obj._entry;
    var me = this;
    request.open("GET", url, true);
    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        var json = JSON.parse(this.response);
        me.setState({
          data:json.data,
        });
      } else {
        Window.alert("ERROR! Cannot connect to server.");
      }
    };
    request.send();
  }
  componentDidMount() {
    $("#example").DataTable({
      processing: true,
      paging: false,
      deferLoading: 100, //to be defined.
      searching: false,
      columnDefs: [
        {
          className: "id",
          targets: [0]
        }
      ]
    });
    var json_obj = {
      _page: "1",
      _entry: "10"
    }; //default
    this.populate_table(json_obj);
  }

  render() {
    
    return (
      <div>
        <HeaderPages pageHeader="Employees Dataset" />
        <div id="form">
          <nav id="search_form" className="navbar navbar-default col-md-8">
            <div className="container-fluid">
              <Searchbar />
              <div
                className="collapse navbar-collapse"
                id="bs-example-navbar-collapse-1"
              >
                <Droplist index="1" dropName="First_Name" />
                <Droplist index="2" dropName="Filter by" />
                <Input />
                <Droplist index="3" dropName="Remove" />
                <ButtonModel
                  ModelTitle="ALERT!"
                  ModelSubmit="YES,I'm totally sure"
                />
                <a href="http://localhost:3000/catalog/projdr"> <button id="add_new" type="button" class="btn btn-default" data-toggle="modal">
                <i class="fa fa-plus"></i> Add New Entry
                </button></a>
              </div>
            </div>
          </nav>
        </div>
        <div class="wrapper" style ={wrapperStyle}>
          <table
            id="example"
            className="table table-striped table-bordered"
            align="center"
          >
            <thead>
              <tr>
                <th className="id">ID</th>
                <th className="text-left">Employee</th>
              </tr>
            </thead>
            <tbody>
              <Table data={this.state.data} />
            </tbody>
          </table>
          <PagingElement />
        </div>
      </div>
    );
  }

}
class Employee extends Component {
  render() {
    return <PageBody />;
  }
}

export default Employee;
