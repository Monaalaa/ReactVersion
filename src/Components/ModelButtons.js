import React, { Component } from "react";
import ModelHeader from './ModelH';
import ModelFooter from './ModelF';
import ModelBody from './ModelB';

class ButtonModel extends Component {
  render() {
    return (
      <ul className="nav navbar-nav">
        <li className="dropdown">
          <button
            id="submitButton"
            type="button"
            className="btn btn-default"
            data-toggle="modal"
            data-target="#exampleModalCenter"
          >
            <b>Delete</b>
          </button>
          <div
            className="modal fade"
            id="exampleModalCenter"
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
              <ModelHeader headerTitle={this.props.ModelTitle} />
                <ModelBody />
                <ModelFooter footerButton={this.props.ModelSubmit}/>
              </div>
            </div>
          </div>
          <div id="snackbar">Selected entries deleted successfully..</div>
        </li>

      </ul>
    );
  }
}
export default ButtonModel;