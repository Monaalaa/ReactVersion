import React, { Component } from "react";

class ModelBody extends Component {

  render() {
    return (
      <div className="modal-body">
        <h id="alert">
            ALERT! Are you sure you want to permenantly delete selected
            employee/s?
        </h>
      </div>
    );
  }
}
export default ModelBody;