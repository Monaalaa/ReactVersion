
import React, { Component } from 'react';
import HeaderPages from './Header';
import './Home.css';
import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
                    <div id="written" align="center">
                        <HeaderPages pageHeader="Company Dataset"/>
                        <Link to = "/Employee">
                        <button id="employee" type="button" className="btn btn-primary">
                           <b> Employees</b>
                        </button>
                        </Link>
                        <br></br>
                        <Link to="/Project">
                        <button id="project" type="button" className="btn btn-primary">
                            <b>Projects</b>
                        </button>
                        </Link>
                    </div>
        );
    }
}

export default Home;
