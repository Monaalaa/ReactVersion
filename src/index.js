import React from 'react';
import ReactDOM from 'react-dom';
import Home from './Components/Home.js';
import Employee from './Components/Employee.js';
import Project from './Components/Project.js';
import CVPage from './Components/CVPage.js';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import './index.css';

ReactDOM.render(
   <Router>
    <div>
    <Route exact path="/" component={Home} />
    <Route path="/Employee" component={Employee} />
    <Route path="/Project" component={Project} />
    <Route path="/user/:id" component={CVPage}/>
    </div>
  </Router>,
  document.getElementById('root')
);

